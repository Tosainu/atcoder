import Control.Applicative ((<$>))
 
fact :: (Num a, Enum a) => a -> [a]
fact n = 1:scanl1 (*) [1..n]
 
main :: IO ()
main = do
  w:h:_ <- map (subtract 1) <$> map read <$> words <$> getLine
  let fs = fact (w + h)
      route  = fs !! (w + h) `quot` ((fs !! w) * (fs !! h))
      answer = route `mod` 1000000007
  print answer
