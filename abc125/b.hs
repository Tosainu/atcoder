import           Data.Array
import           Data.List

type Value = Int
type Cost = Int
type Score = Int
type Gem = (Value, Cost)

score :: Gem -> Score
score (v, c) = v - c

solve :: [Score] -> Score
solve scores = dp ! imax
  where
    dp = array (0, imax) [(i, f i) | i <- [0..imax]]
    f 0 = 0
    f i = let s = scores !! (i - 1) -- i番目のスコア
          in  maximum [ s                 -- i 番目だけ買う
                      , dp ! (i - 1)      -- i 番目を買わない
                      , dp ! (i - 1) + s  -- i 番目も買う
                      ]
    imax = length scores

main :: IO ()
main = do
  _ <- getLine -- N
  vs <- map read . words <$> getLine :: IO [Score]
  cs <- map read . words <$> getLine :: IO [Score]

  let scores = zipWith (curry score)  vs cs

  print $ solve scores
