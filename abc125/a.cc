#include <iostream>

auto main() -> int {
  int A, B, T;
  std::cin >> A >> B >> T;

  int O = (T / A) * B;

  std::cout << O << std::endl;
}
