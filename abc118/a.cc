#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

constexpr std::int64_t INF = 1ull << 50;

auto main() -> int {
  int A, B;
  std::cin >> A >> B;
  int ans = (B % A == 0) ? A + B : B - A;
  std::cout << ans << '\n';
}
