#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

int f(int i) {
  switch (i) {
    case 1:
      return 2;
    case 7:
      return 3;
    case 4:
      return 4;
    case 2:
    case 3:
    case 5:
      return 5;
    case 6:
    case 9:
      return 6;
    case 8:
      return 7;
    default:
      return INF;
  }
}

auto main() -> int {
  int N, M;
  std::cin >> N >> M;

  std::vector<int> As(M);
  for (auto i = 0; i < M; ++i) {
    std::cin >> As[i];
  }
  std::sort(As.begin(), As.end(), std::greater<int>{});

  // dp[i][0]: i 本のマッチを使い切って何桁の数が作れるか (-1: 作れない)
  // dp[i][j]: そのとき j がいくつ作れるか
  std::vector<std::array<int, 10>> dp(N + 1);
  dp[0][0] = 0;
  dp[1][0] = -1;

  for (auto i = 2; i <= N; ++i) {
    // i - f(As[j]) 本のマッチで作れる数字の最大桁数
    int mi = 0, ma = -1, max = -1;
    for (const auto Ai : As) {
      int ti = i - f(Ai);
      if (ti < 0) continue;
      if (dp[ti][0] > max) {
        max = dp[ti][0];
        mi = ti;
        ma = Ai;
      }
    }

    if (ma != -1) {
      dp[i] = dp[mi];
      dp[i][0]++;
      dp[i][ma]++;
    } else {
      dp[i][0] = -1;
    }

    // for (auto j = 0ul; j < 10; ++j) {
    //   std::cout << j << ' ' << dp[i][j] << std::endl;
    // }
    // std::cout << "--------------------------------" << std::endl;
  }

  for (auto i = 9ul; i > 0ul; --i) {
    for (auto j = 0; j < dp[N][i]; ++j) std::cout << i;
  }
  std::cout << std::endl;
}
