#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

constexpr std::int64_t INF = 1ull << 50;

auto main() -> int {
  int N, M;
  std::cin >> N >> M;

  std::vector<int> t(M, 0);
  for (auto i = 0; i < N; ++i) {
    int Ki;
    std::cin >> Ki;
    for (auto j = 0; j < Ki; ++j) {
      int Aij;
      std::cin >> Aij;
      t[Aij - 1]++;
    }
  }

  const auto ans = std::count(t.cbegin(), t.cend(), N);
  std::cout << ans << '\n';
}
