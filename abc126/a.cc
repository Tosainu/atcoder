#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  std::uint64_t N, K;
  std::cin >> N >> K;

  std::string S;
  std::cin >> S;

  S[K - 1] = std::tolower(S[K - 1]);

  std::cout << S << std::endl;
}
