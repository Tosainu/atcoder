#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

bool y(int i) {
  return i >= 0 && i <= 99;
}

bool m(int i) {
  return i > 0 && i <= 12;
}

auto main() -> int {
  std::string S;
  std::cin >> S;

  const auto a = std::stoi(S.substr(0, 2));
  const auto b = std::stoi(S.substr(2, 2));

  const auto yymm = y(a) && m(b);
  const auto mmyy = m(a) && y(b);

  if (yymm && mmyy) {
    std::cout << "AMBIGUOUS" << std::endl;
  } else if (yymm) {
    std::cout << "YYMM" << std::endl;
  } else if (mmyy) {
    std::cout << "MMYY" << std::endl;
  } else {
    std::cout << "NA" << std::endl;
  }
}
