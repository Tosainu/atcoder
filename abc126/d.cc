#include <algorithm>
#include <bitset>
#include <cstdint>
#include <deque>
#include <iostream>
#include <memory>
#include <queue>
#include <tuple>
#include <vector>

using edge_type = std::tuple<std::uint32_t, // v (to)
                             bool           // even
                             >;

enum class node_type {
  // invalid,
  white,
  black,
};

node_type inv(node_type n) {
  switch (n) {
    // case node_type::invalid:
    //   return node_type::invalid;
    case node_type::white:
      return node_type::black;
    case node_type::black:
      return node_type::white;
  }
}

auto main() -> int {
  std::uint64_t N;
  std::cin >> N;

  // 辺
  std::vector<std::vector<edge_type>> es(N);
  for (auto i = 0ul; i < N - 1ul; ++i) {
    std::uint32_t u, v;
    std::uint64_t w;
    std::cin >> u >> v >> w;
    es[u - 1].emplace_back(v - 1, w % 2 == 0);
  }

  // 頂点
  std::vector<node_type> ns(N, node_type::white);

  for (auto i = 0ul; i < N; ++i) {
    for (const auto& e : es[i]) {
      const auto v = std::get<0>(e);
      const auto w = std::get<1>(e);
      ns[v] = w ? ns[i] : inv(ns[i]);
    }
  }

  for (auto n : ns) {
    switch (n) {
      case node_type::white:
        std::cout << "0\n";
        break;
      case node_type::black:
        std::cout << "1\n";
        break;
    }
  }
}
