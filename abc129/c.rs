use std::cmp;
use std::collections::*;
use std::i32;
use std::io::{self, Read};
use std::mem;

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let a: Vec<usize> = sc.read_vec(m).unwrap();
    let mut a = VecDeque::from(a);

    // t[i] = i 段目に行く登り方のパターン
    let mut t = Vec::with_capacity(n + 1);
    t.push(1 as usize);
    for i in 1..n + 1 {
        // i 段目が壊れていたら 0
        let mut zero = false;
        if let Some(ai) = a.front() {
            if ai == &i {
                zero = true;
            }
        }
        if zero {
            a.pop_front();
            t.push(0);
            // println!("{} = 0", i);
            continue;
        }

        let n: usize;
        if i >= 2 {
            n = (t[i - 1] + t[i - 2]) % MOD;
        } else {
            n = t[i - 1] % MOD;
        }
        // println!("{} = {}", i, n);
        t.push(n);
    }

    println!("{}", t[n]);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
