use std::cmp;
use std::collections::*;
use std::i32;
use std::io::{self, Read};
use std::mem;

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let h: usize = sc.read().unwrap();
    let w: usize = sc.read().unwrap();
    let b: Vec<Vec<char>> = sc.read_board(h).unwrap();

    // // h 行目の障害物の数
    // let mut ho = Vec::with_capacity(h);
    // // w 列目の障害物の数
    // let mut wo = vec![0 as usize; w];
    // for i in 0..h {
    //     let mut l: usize = 0;
    //     for j in 0..w {
    //         if b[i][j] == '#' {
    //             l = l + 1;
    //             wo[j] = wo[j] + 1;
    //         }
    //     }
    //     ho.push(l);
    // }
    // println!("ho = {:?}", ho);
    // println!("wo = {:?}", wo);

    let mut t = Vec::with_capacity(h);
    // 横方向の探索
    for i in 0..h {
        t.push(vec![0 as usize; w]);

        let mut j: usize = 0;
        while j < w {
            // 横方向の障害物がないマスを数える
            let mut jj: usize = j;
            while jj < w && b[i][jj] == '.' {
                jj = jj + 1;
            }

            for k in j..jj {
                t[i][k] = jj - j;
            }
            j = jj + 1;
        }
    }

    // 縦方向の探索
    for j in 0..w {
        let mut i: usize = 0;
        while i < h {
            // 横方向の障害物がないマスを数える
            let mut ii: usize = i;
            while ii < h && b[ii][j] == '.' {
                ii = ii + 1;
            }

            for k in i..ii {
                t[k][j] = t[k][j] + (ii - i);
            }
            i = ii + 1;
        }
    }

    // println!("{:?}", t);

    let mut max: usize = 0;
    for i in 0..h {
        for j in 0..w {
            max = cmp::max(t[i][j], max);
        }
    }

    println!("{}", max - 1);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
