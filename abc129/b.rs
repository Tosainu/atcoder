use std::cmp;
use std::collections::*;
use std::i32;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let ws: Vec<i32> = sc.read_vec(n).unwrap();
    // println!("{:?}", ws);

    // ww[i] = ws[0] ~ ws[i] の和
    let mut ww = Vec::with_capacity(n);
    ww.push(ws[0]);
    for i in 1..n {
        let wwp = ww[i - 1];
        ww.push(wwp + ws[i]);
    }
    // println!("{:?}", ww);

    // wp[i] = ws[i] で分割したときの差
    let mut wp = Vec::with_capacity(n);
    wp.push(1000000); // wp[0] は対象外
    for i in 1..n {
        // let wpi = ww[i - 0] - (ww[n - 1] - ww[i - 1]);
        let wpi = (ww[n - 1] - ww[i - 1] * 2).abs();
        wp.push(wpi);
    }
    // println!("{:?}", wp);

    println!("{}", wp.iter().min().unwrap());
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
