#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

std::pair<std::int64_t, std::int64_t> f(std::int64_t a, std::int64_t b) {
  return {-a, b};
}

auto main() -> int {
  std::uint32_t Q;
  std::cin >> Q;

  std::int64_t a = 0, b = 0;
  std::pair<int64_t, std::int64_t> r{};
  for (auto i = 0u; i < Q; ++i) {
    int cmd;
    std::cin >> cmd;

    switch (cmd) {
      case 1:
        std::cin >> a >> b;
        break;
      case 2:
        r = f(a, b);
        std::cout << r.first << ' ' << r.second << '\n';
        break;
    }
  }
}
