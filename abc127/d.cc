#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint64_t N, M;
  std::cin >> N >> M;

  std::vector<std::uint64_t> As(N);
  for (auto& Ai : As) {
    std::cin >> Ai;
  }
  std::sort(As.begin(), As.end());

  using cibi = std::pair<std::uint64_t, std::uint64_t>; // <Ci, Bi>
  std::priority_queue<cibi, std::vector<cibi>> q{};
  for (auto i = 0ul; i < M; ++i) {
    std::uint64_t Bi, Ci;
    std::cin >> Bi >> Ci;
    q.emplace(Ci, Bi);
  }

  std::size_t cur = 0;
  std::uint64_t ans = 0;

  while (!q.empty()) {
    const auto Bi = q.top().second;
    const auto Ci = q.top().first;
    q.pop();

    // これ以上最大化できないなら終了
    if (Ci < As[cur]) {
      break;
    }

    // std::cout << "> " << Bi << ' ' << Ci << ' ';

    // 最大 Bi 枚 Ci に置き換える
    auto i = 0ul;
    for (; i < Bi && (cur + i) < N && As[cur + i] < Ci; ++i)
      ;
    ans += Ci * i;
    // std::cout << '(' << i << ')' << std::endl;

    cur += i;
  }

  // std::cout << cur << ' ' << ans << std::endl;

  // 残りを足す
  ans = std::accumulate(As.cbegin() + cur, As.cend(), ans);

  std::cout << ans << std::endl;
}
