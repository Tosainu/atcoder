#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int A, B;
  std::cin >> A >> B;

  if (A >= 13) {
    std::cout << B << std::endl;
  } else if (A <= 5) {
    std::cout << 0 << std::endl;
  } else {
    std::cout << (B / 2) << std::endl;
  }
}
