use std::cmp;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: i32 = sc.read().unwrap();
    let m: i32 = sc.read().unwrap();

    let mut min = -1;
    let mut max = 1000000;
    for _ in (0..m) {
        let li: i32 = sc.read().unwrap();
        let ri: i32 = sc.read().unwrap();

        min = cmp::max(li, min);
        max = cmp::min(ri, max);
    }

    if max - min < 0 {
        println!("{}", 0);
    } else {
        println!("{}", max - min + 1);
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }
}
