#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N, M;
  std::cin >> N >> M;

  int s = -1, t = INF;
  for (auto i = 0; i < M; ++i) {
    int Li, Ri;
    std::cin >> Li >> Ri;
    s = std::max(Li, s);
    t = std::min(Ri, t);
  }

  if (t - s < 0) {
    std::cout << 0 << std::endl;
  } else {
    std::cout << (t - s + 1) << std::endl;
  }
}
