#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint64_t r, D, x;
  std::cin >> r >> D >> x;

  for (auto i = 0u; i < 10; ++i) {
    x = r * x - D;
    std::cout << x << '\n';
  }
}
