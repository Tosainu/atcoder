#![allow(unused_imports)]
use std::cmp::{self, Ord, Ordering};
use std::collections::{BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque};

fn main() {
    let stdin = std::io::stdin();
    let mut sc = nyan::Scanner::new(stdin.lock());

    let n: usize = sc.read();
    let w: usize = sc.read();
    let p = (0..n)
        .map(|_i| {
            let wi: u64 = sc.read();
            let vi: u64 = sc.read();
            (wi, vi)
        })
        .collect::<Vec<_>>();

    let mut dp = vec![vec![0; w + 1]; n + 1];
    for j in 1..w + 1 {
        for i in 1..n + 1 {
            let (wi, vi) = p[i - 1];
            if wi as usize > j {
                dp[i][j] = dp[i - 1][j];
            } else {
                dp[i][j] = cmp::max(dp[i - 1][j], vi + dp[i - 1][j - wi as usize]);
            }
        }
    }

    println!("{}", dp[n][w]);
}

#[allow(dead_code)]
mod nyan {
    use std::cmp::*;
    use std::fmt::Debug;
    use std::io::Read;
    use std::iter::FromIterator;
    use std::str::FromStr;

    pub struct Scanner<R: Read> {
        reader: R,
    }

    impl<R: Read> Scanner<R> {
        pub fn new(reader: R) -> Scanner<R> {
            Scanner { reader: reader }
        }

        fn chars<'a>(&'a mut self) -> Box<Iterator<Item = char> + 'a> {
            Box::new(self.reader.by_ref().bytes().map(|b| b.unwrap() as char))
        }

        // since Rust 1.26
        // fn chars<'a>(&'a mut self) -> impl Iterator<Item = char> + 'a {
        //     self.reader.by_ref().bytes().map(|b| b.unwrap() as char)
        // }

        pub fn next<T: FromIterator<char>>(&mut self) -> T {
            self.chars()
                .skip_while(|c| c.is_whitespace())
                .take_while(|c| !c.is_whitespace())
                .collect()
        }

        pub fn next_line<T: FromIterator<char>>(&mut self) -> T {
            self.chars()
                .skip_while(|c| c.is_whitespace())
                .take_while(|c| !c.is_whitespace() || c == &' ')
                .collect()
        }

        pub fn read<T>(&mut self) -> T
        where
            T: FromStr,
            <T as FromStr>::Err: Debug,
        {
            self.next::<String>().parse().unwrap()
        }

        pub fn read_vec<T, U>(&mut self, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromStr,
            <U as FromStr>::Err: Debug,
        {
            (0..n).map(|_| self.read()).collect()
        }

        pub fn read_vec2d<T, U, V>(&mut self, m: usize, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromIterator<V>,
            V: FromStr,
            <V as FromStr>::Err: Debug,
        {
            (0..m).map(|_| self.read_vec(n)).collect()
        }

        pub fn read_line<T: FromIterator<char>>(&mut self) -> T {
            self.next_line()
        }

        pub fn read_lines<T, U>(&mut self, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromIterator<char>,
        {
            (0..n).map(|_| self.read_line()).collect()
        }
    }

    // https://doc.rust-lang.org/std/cmp/struct.Reverse.html
    #[derive(PartialEq, Eq, Debug)]
    pub struct Rev<T>(pub T);

    impl<T: PartialOrd> PartialOrd for Rev<T> {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            other.0.partial_cmp(&self.0)
        }
    }

    impl<T: Ord> Ord for Rev<T> {
        fn cmp(&self, other: &Self) -> Ordering {
            other.0.cmp(&self.0)
        }
    }

    pub fn gcd(a: u64, b: u64) -> u64 {
        if b == 0 {
            a
        } else {
            gcd(b, a % b)
        }
    }

    // Extended Euclidean algorithm
    // ax + by = gcd(x, y)
    pub fn extended_gcd(a: u64, b: u64) -> (u64, i64, i64) {
        if b == 0 {
            (a, 1, 0)
        } else {
            let (m, y, x) = extended_gcd(b, a % b);
            (m, x, y - (a / b) as i64 * x)
        }
    }

    pub fn lcm(a: u64, b: u64) -> u64 {
        (a * b) / gcd(a, b)
    }

    pub struct UnionFind {
        n: usize,
        root: Vec<usize>,
        rank: Vec<usize>,
        size: Vec<usize>,
    }

    impl UnionFind {
        pub fn new(n: usize) -> UnionFind {
            UnionFind {
                n: n,
                root: (0..n).collect(),
                rank: vec![0; n],
                size: vec![1; n],
            }
        }

        pub fn root(&mut self, x: usize) -> usize {
            let r = self.root[x];
            if r == x {
                r
            } else {
                self.root[x] = self.root(r);
                self.root[x]
            }
        }

        pub fn merge(&mut self, x: usize, y: usize) {
            let rx = self.root(x);
            let ry = self.root(y);
            if rx == ry {
                return;
            }

            if self.rank[rx] < self.rank[ry] {
                self.root[rx] = ry;
                self.size[ry] += self.size[rx];
            } else {
                self.root[ry] = rx;
                self.size[rx] += self.size[ry];
                if self.rank[rx] == self.rank[ry] {
                    self.rank[rx] += 1;
                }
            }
        }

        pub fn size(&mut self, x: usize) -> usize {
            let rx = self.root(x);
            self.size[rx]
        }
    }
}
