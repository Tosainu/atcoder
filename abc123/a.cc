#include <algorithm>
#include <iostream>

constexpr auto N = 5;

auto main() -> int {
  int ps[N];
  for (int i = 0; i < N; ++i) {
    std::cin >> ps[i];
  }

  int k;
  std::cin >> k;

  for (auto i = 0; i < N - 1; ++i) {
    for (auto j = i + 1; j < N; ++j) {
      if (ps[j] - ps[i] > k) {
        std::cout << ":(\n";
        return 0;
      }
    }
  }

  std::cout << "Yay!\n";
}
