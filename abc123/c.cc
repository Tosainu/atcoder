#include <algorithm>
#include <cstdint>
#include <iostream>

auto main() -> int {
  std::int64_t N;
  std::cin >> N;

  std::int64_t ms[5];
  for (int i = 0; i < 5; ++i) {
    std::cin >> ms[i];
  }

  const auto m = std::min_element(std::begin(ms), std::end(ms));

  std::cout << (((N - 1) / *m) + 5) << "\n";
}

// std::int64_t i = 0;
// for (; ps[5] != N; ++i) {
//   for (int j = 4; j >= 0; --j) {
//     if (ps[j] > 0) {
//       const auto s = std::min(ms[j], ps[j]);
//       ps[j + 1] += s;
//       ps[j] -= s;
//     }
//   }

//   std::cout << "> ";
//   for (auto p : ps) {
//     std::cout << p << ' ';
//   }
//   std::cout << std::endl;
// // }
