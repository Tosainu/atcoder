use std::cmp;
use std::collections::*;
use std::i32;
use std::io::{self, Read};
use std::mem;

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let k: usize = sc.read().unwrap();

    // 2: 1 * 0 = 0, ()
    // 3: 1 * 2 = 2, (2, 1), (2, 3)
    // 4: 2 * 2 = 4, (2, 1), (2, 3), (4, 1), (4, 3)
    // 5: 2 * 3 = 6
    // 6: 3 * 3 = 9
    // 7: 3 * 4 = 12
    // 8: 4 * 4 = 16
    // 9: 4 * 5 = 20
    // 10: 5 * 5 = 25
    // 11: 5 * 6 = 30
    // n: (n / 2) * ((n / 2) + (n % 2))

    // println!("{}", (k / 2) * ((k / 2) + (k % 2)));
    println!("{}", (k / 2) * ((k + 1) / 2));
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
