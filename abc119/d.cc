#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

constexpr std::int64_t INF = 1ull << 50;

auto main() -> int {
  int A, B, Q;
  std::cin >> A >> B >> Q;

  // 神社
  std::vector<std::int64_t> ss(A);
  for (auto i = 0; i < A; ++i) {
    std::cin >> ss[i];
  }
  // 寺
  std::vector<std::int64_t> ts(B);
  for (auto i = 0; i < B; ++i) {
    std::cin >> ts[i];
  }

  for (auto i = 0; i < Q; ++i) {
    std::int64_t xi;
    std::cin >> xi;

    // xi から近い神社
    std::int64_t s1, s2;
    {
      const auto i1 = std::upper_bound(ss.cbegin(), ss.cend(), xi);
      const auto i2 = i1 - 1;
      s1 = i1 >= ss.cend() ? INF : *i1;
      s2 = i2 < ss.cbegin() ? INF : *i2;
    }

    // xi から近い寺
    std::int64_t t1, t2;
    {
      const auto i1 = std::upper_bound(ts.cbegin(), ts.cend(), xi);
      const auto i2 = i1 - 1;
      t1 = i1 >= ts.cend() ? INF : *i1;
      t2 = i2 < ts.cbegin() ? INF : *i2;
    }

    const auto s1t1 = std::abs(s1 - t1);
    const auto s1t2 = std::abs(s1 - t2);
    const auto s2t1 = std::abs(s2 - t1);
    const auto s2t2 = std::abs(s2 - t2);

    const auto ans = std::min({
        std::abs(xi - s1) + s1t1,
        std::abs(xi - s1) + s1t2,
        std::abs(xi - s2) + s2t1,
        std::abs(xi - s2) + s2t2,
        std::abs(xi - t1) + s1t1,
        std::abs(xi - t1) + s2t1,
        std::abs(xi - t2) + s1t2,
        std::abs(xi - t2) + s2t2,
    });
    std::cout << ans << '\n';
    // std::cout << "> " << xi << ' ' << ans << '\n';
  }
}
