#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  std::size_t N;
  std::cin >> N;

  double ans = 0;
  for (auto i = 0ul; i < N; ++i) {
    double xi;
    std::string ui{};
    std::cin >> xi >> ui;

    if (ui == "JPY") {
      ans += xi;
    } else {
      ans += xi * 380000.0;
    }
  }

  std::cout << ans << std::endl;
}
