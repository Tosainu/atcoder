#include <algorithm>
#include <array>
#include <bitset>
#include <functional>
#include <cstdint>
#include <deque>
#include <iostream>
#include <memory>
#include <limits>
#include <vector>

auto main() -> int {
  int N, A, B, C;
  std::cin >> N >> A >> B >> C;

  // N 本の竹
  std::vector<int> ls(N);
  for (auto i = 0; i < N; ++i) {
    std::cin >> ls[i];
  }

  std::function<int(int, int, int, int)> f = [&f, N, A, B, C, &ls](int i, int a, int b, int c) {
    if (i == N) {
      if (std::min({a, b, c}) > 0)
        return std::abs(a - A) + std::abs(b - B) + std::abs(c - C);
      else
        return 10000000;
    }

    const auto r1 = f(i + 1, a, b, c);                        // l[i] を使わない
    const auto r2 = f(i + 1, a + ls[i], b, c) + (a ? 10 : 0); // A の材料にする
    const auto r3 = f(i + 1, a, b + ls[i], c) + (b ? 10 : 0); // B の材料にする
    const auto r4 = f(i + 1, a, b, c + ls[i]) + (c ? 10 : 0); // C の材料にする

    return std::min({r1, r2, r3, r4});
  };

  std::cout << f(0, 0, 0, 0) << std::endl;
}
