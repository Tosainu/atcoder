#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  std::string S;
  std::cin >> S;

  const auto y = std::stoi(S.substr(0, 4));
  const auto m = std::stoi(S.substr(5, 2));
  const auto d = std::stoi(S.substr(8, 2));

  if (y >= 2019) {
    if (m > 4) {
      std::cout << "TBD\n";
    } else {
      std::cout << "Heisei\n";
    }
  } else {
    std::cout << "Heisei\n";
  }
}
