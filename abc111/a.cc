#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::string n{};
  std::cin >> n;

  for (auto c : n) {
    if (c == '1')
      std::cout << '9';
    else if (c == '9')
      std::cout << '1';
    else
      std::cout << c;
  }
  std::cout << std::endl;
}
