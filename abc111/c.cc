#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

static std::array<std::uint32_t, 100000> as{};
static std::array<std::uint32_t, 100000> bs{};

auto main() -> int {
  std::uint32_t n;
  std::cin >> n;

  for (auto i = 0ul; i < n; ++i) {
    std::uint32_t vi;
    std::cin >> vi;
    if (i & 1u)
      as[vi]++;
    else
      bs[vi]++;
  }

  using pt = std::pair<std::uint32_t, std::uint32_t>;
  std::vector<pt> ap{{0, INF}};
  std::vector<pt> bp{{0, INF}};
  for (auto i = 0ul; i < as.size(); ++i) {
    if (as[i] > 0) ap.emplace_back(as[i], i);
    if (bs[i] > 0) bp.emplace_back(bs[i], i);
  }
  std::sort(ap.begin(), ap.end(), std::greater<pt>{});
  std::sort(bp.begin(), bp.end(), std::greater<pt>{});

  if (ap[0].second != bp[0].second) {
    std::cout << (n - ap[0].first - bp[0].first) << std::endl;
  } else {
    std::cout << std::min(n - ap[1].first - bp[0].first, n - ap[0].first - bp[1].first)
              << std::endl;
  }
}
