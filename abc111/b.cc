#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N;
  std::cin >> N;

  const int n3 = N / 100;

  if ((n3 * 111) - N >= 0) {
    std::cout << (n3 * 111) << std::endl;
  } else {
    std::cout << ((n3 + 1) * 111) << std::endl;
  }
}
