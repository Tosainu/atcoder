use std::cmp::{self, Ordering};
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let k: usize = sc.read().unwrap();
    let a: Vec<usize> = sc.read_vec(n).unwrap();

    let t: Vec<usize> = a
        .iter()
        .scan(0, |state, &x| {
            *state += x;
            Some(*state)
        })
        .collect();

    // println!("{:?}", t);

    let mut js = 0;
    for i in 0..n {
        let jj = n - i - 1;
        if t[jj] < k {
            js = jj + 1;
            break;
        }
    }

    // println!("js: {}", js);

    let mut ans = if js == 0 { n } else { n - js };

    for i in 0..n {
        for j in js..n {
            // println!("{} {}: {}", i, j, t[j] - t[i]);

            if t[j] - t[i] >= k {
                // print!("ans: {} -> ", ans);
                ans += n - j;
                // println!("{}", ans);
                js = j;
                break;
            }
        }
    }

    println!("{}", ans);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
