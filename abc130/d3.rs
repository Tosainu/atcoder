use std::cmp;
use std::collections::*;
use std::io::{self, Read};

#[allow(dead_code)]
const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let k: usize = sc.read().unwrap();
    let a: Vec<usize> = sc.read_vec(n).unwrap();

    let t: Vec<usize> = a
        .iter()
        .scan(0, |state, &x| {
            *state += x;
            Some(*state)
        })
        .collect();

    // println!("{:?}", t);

    let mut js = n;
    for i in 0..n {
        if t[i] >= k {
            js = i;
            break;
        }
    }

    let mut ans = n - js;
    // println!("js: {}, ans: {}", js, ans);

    for i in 0..n - 1 {
        for j in cmp::max(js, i + 1)..n {
            // println!("{} {}: {}", i, j, t[j] - t[i]);

            if t[j] - t[i] >= k {
                // print!("ans: {} -> ", ans);
                ans += n - j;
                // println!("{}", ans);
                js = j;
                break;
            }
        }
    }

    println!("{}", ans);
}

struct Scanner<R: Read> {
    reader: R,
}

#[allow(dead_code)]
impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
