#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint64_t N;
  std::cin >> N;

  std::uint64_t ans = 0;
  for (auto i = 0ull; i < N; ++i) {
    std::uint64_t Ai, Bi;
    std::cin >> Ai >> Bi;
    ans += std::min(Ai / 2, Bi);
  }
  std::cout << ans << std::endl;
}
