#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint64_t N;
  std::cin >> N;

  std::vector<std::tuple<std::uint64_t, std::uint64_t>> v{};
  for (auto i = 0ull; i < N; ++i) {
    std::uint64_t Ai, Bi;
    std::cin >> Ai >> Bi;
    v.emplace_back(std::min(Ai, Bi), std::max(Ai, Bi));
  }

  std::sort(v.begin(), v.end());
  const auto last = std::unique(v.begin(), v.end());

  std::cout << (last - v.begin()) << std::endl;
}
