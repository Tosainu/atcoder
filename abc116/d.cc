#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

using value_type = std::pair<std::uint64_t,                    // おいしさ基礎ポイント
                             std::unordered_set<std::uint64_t> // 種類数
                             >;

std::uint64_t manzoku(const value_type& v) {
  // 満足ポイント = おいしさ基礎ポイント + 種類ボーナスポイント
  // 種類ボーナスポイント = 種類数 * 種類数
  const auto v1s = std::get<1>(v).size();
  return std::get<0>(v) + (v1s * v1s);
}

auto main() -> int {
  std::uint32_t N, K;
  std::cin >> N >> K;

  // <種類, おいしさ>
  std::vector<std::pair<std::uint32_t, std::uint32_t>> tds(N);
  for (auto& td : tds) {
    std::cin >> td.first >> td.second;
  }

  // dp[i][j]: j 番目までの🍣から i = k % 2 個選ぶときの満足ポイントの最大値
  std::array<std::vector<value_type>, 2> dp{std::vector<value_type>(N + 1),
                                            std::vector<value_type>(N + 1)};

  for (auto k = 1ul; k <= K; ++k) {
    auto& dpi = dp.at(k % 2);       // dp[k]
    auto& dpp = dp.at((k + 1) % 2); // dp[k - 1]

    // k 個の🍣 を選ぶには k 個以上の🍣が必要
    for (auto j = k; j <= N; ++j) {
      // j 番目の🍣を選ぶときのスコア
      auto r1 = dpp[j - 1];
      r1.first += tds[j - 1].second;
      r1.second.emplace(tds[j - 1].first);

      // j 番目の🍣を選ばないときのスコア
      auto r2 = j > k ? dpi[j - 1] : value_type{};

      dpi[j] = manzoku(r1) > manzoku(r2) ? r1 : r2;
    }
  }

  std::cout << manzoku(dp[K % 2][N]) << std::endl;
}
