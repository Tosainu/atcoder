#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint32_t N;
  std::cin >> N;

  std::vector<std::uint32_t> hs(N);
  for (auto& hi : hs) {
    std::cin >> hi;
  }

  const auto max_h = *std::max_element(hs.cbegin(), hs.cend());

  int ans = 0;
  for (auto i = 1u; i <= max_h; ++i) {
    auto it = hs.cbegin();
    while (it < hs.cend()) {
      for (; it < hs.cend() && *it < i; ++it)
        ;

      bool f = false;
      for (; it < hs.cend() && *it >= i; ++it) {
        f = true;
      }
      if (f) ans++;
    }
  }

  std::cout << ans << std::endl;
}
