#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

std::uint64_t f(std::uint64_t n) {
  if (n % 2) {
    return n * 3 + 1;
  } else {
    return n / 2;
  }
}

auto main() -> int {
  std::uint64_t s;
  std::cin >> s;

  std::array<std::uint64_t, 2> as{};
  as.at(0) = s;

  std::unordered_set<std::uint64_t> m{};
  m.emplace(s);

  for (auto i = 1ul;; ++i) {
    auto& ai = as.at(i % 2);
    auto& aip = as.at((i + 1) % 2);

    ai = f(aip);
    // std::cout << i << ' ' << "f(" << aip << ") = " << ai << std::endl;

    if (m.count(ai)) {
      std::cout << (i + 1) << std::endl;
      break;
    } else {
      m.emplace(ai);
    }
  }
}
