#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N;
  std::cin >> N;

  std::vector<int> Ls(N);
  for (auto& Li : Ls) {
    std::cin >> Li;
  }
  std::sort(Ls.begin(), Ls.end(), std::greater<int>{});

  const auto l = Ls.front();
  const auto m = std::accumulate(Ls.cbegin() + 1, Ls.cend(), 0);

  std::cout << (l < m ? "Yes" : "No") << std::endl;
}
