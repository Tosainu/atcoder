#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N, M;
  std::cin >> N >> M;

  std::vector<int> Xs(M);
  for (auto& Xi : Xs) {
    std::cin >> Xi;
  }

  if (N > M) {
    std::cout << 0 << std::endl;
    return 0;
  }

  std::sort(Xs.begin(), Xs.end());

  std::vector<int> Ls(M - 1);
  for (int i = 0; i < M - 1; ++i) {
    Ls[i] = std::abs(Xs[i + 1] - Xs[i]);
  }
  std::sort(Ls.begin(), Ls.end(), std::greater<int>{});

  const auto ans = std::accumulate(Ls.cbegin() + (N - 1), Ls.cend(), 0);

  std::cout << ans << std::endl;
}
