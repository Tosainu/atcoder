#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint64_t N, K;
  std::cin >> N >> K;

  std::vector<std::uint64_t> As(N);
  for (auto& Ai : As) {
    std::cin >> Ai;
  }

  std::uint64_t X = 1;
  if (K == 0)
    X = 0;
  else
    while (K >>= 1) X <<= 1;

  const auto ans =
      std::accumulate(As.cbegin(), As.cend(), 0ull, [X](auto a, auto b) { return a + (b ^ X); });

  std::cout << ans << std::endl;
}
