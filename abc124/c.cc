#include <algorithm>
#include <cstdint>
#include <iostream>

auto main() -> int {
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  std::string s{};
  std::cin >> s;

  const auto l = s.length();

  // 01010101... か
  // 10101010... にすればよさそう？
  std::int64_t bw = 0;
  std::int64_t wb = 0;

  constexpr char c[] = {'0', '1'};

  for (auto i = 0ul; i < l; ++i) {
    if (s[i] != c[i % 2]) bw++;
    if (s[i] != c[(i + 1) % 2]) wb++;
  }

  std::cout << std::min(bw, wb) << std::endl;
}
