#include <algorithm>
#include <iostream>

auto main() -> int {
  int a, b;
  std::cin >> a >> b;

  const auto c1 = std::minmax(a, b);
  const auto c2 = std::max(c1.second - 1, c1.first);

  std::cout << c1.second + c2 << std::endl;
}
