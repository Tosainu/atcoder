#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N, T, A;
  std::cin >> N >> T >> A;

  std::vector<int> Hs(N);
  for (auto& Hi : Hs) {
    std::cin >> Hi;
  }

  double diff = INF;
  int idx = 0;
  for (auto i = 0; i < N; ++i) {
    const auto d = std::abs(0.006 * Hs[i] - T + A);
    if (d < diff) {
      diff = d;
      idx = i;
    }
  }

  std::cout << (idx + 1) << std::endl;
}
