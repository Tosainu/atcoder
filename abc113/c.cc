#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint32_t N, M;
  std::cin >> N >> M;

  // <Pi, Yi, Idx>
  using city_type = std::tuple<std::uint32_t, std::uint64_t, std::uint32_t>;
  std::vector<city_type> Cs(M);
  std::vector<std::vector<std::vector<city_type>::iterator>> CRs(N);
  for (auto it = Cs.begin(); it < Cs.end(); ++it) {
    auto& Pi = std::get<0>(*it);
    auto& Yi = std::get<1>(*it);
    std::cin >> Pi >> Yi;

    CRs.at(Pi - 1).emplace_back(it);
  }

  for (auto& p : CRs) {
    std::sort(p.begin(), p.end(),
              [](auto& a, auto& b) { return std::get<1>(*a) < std::get<1>(*b); });
    std::uint32_t i = 1;
    for (auto& c : p) {
      std::get<2>(*c) = i++;
    }
  }

  for (const auto& c : Cs) {
    std::cout << std::setw(6) << std::setfill('0') << std::get<0>(c);
    std::cout << std::setw(6) << std::setfill('0') << std::get<2>(c) << '\n';
  }
}
