use std::cmp;
use std::collections::*;
use std::i32;
use std::io::{self, Read};
use std::mem;

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let h: usize = sc.read().unwrap();
    let w: usize = sc.read().unwrap();
    let mut b = sc.read_board(h).unwrap();

    loop {
        let mut f = false;

        // 行
        for i in 0..h {
            let mut j = 0;
            let mut l = false;
            while j < w && b[i][j] != '#' {
                l = l || b[i][j] == '.';
                j = j + 1;
            }

            if l && j == w {
                for jj in 0..w {
                    b[i][jj] = '-';
                }
                f = true;
            }
        }

        // 列
        for j in 0..w {
            let mut i = 0;
            let mut l = false;
            while i < h && b[i][j] != '#' {
                l = l || b[i][j] == '.';
                i = i + 1;
            }

            if l && i == h {
                for ii in 0..h {
                    b[ii][j] = '-';
                }
                f = true;
            }
        }

        if !f {
            break;
        }
    }

    for l in b {
        let mut p = false;
        for c in l {
            if c != '-' {
                p = true;
                print!("{}", c);
            }
        }

        if p {
            println!("");
        }
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
