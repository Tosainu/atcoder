use std::cmp;
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn usqrt(u: u32) -> u32 {
    (u as f64).sqrt() as u32
}

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: u32 = sc.read().unwrap();

    let mut ans = 0;
    for ii in 0..(n + 1) / 2 {
        let i = ii * 2 + 1;

        let mut c = 0;
        for j in 1..i + 1 {
            if i % j == 0 {
                c = c + 1;
            }
        }

        if c == 8 {
            ans = ans + 1;
        }
    }
    println!("{}", ans);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
