use std::cmp::{self, Ordering};
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let q: usize = sc.read().unwrap();

    let mut t1 = Vec::with_capacity(n);
    for _ in 0..n {
        t1.push(vec![0 as i32; n]);
    }
    for _ in 0..m {
        let li: usize = sc.read().unwrap();
        let ri: usize = sc.read().unwrap();
        t1[li - 1][ri - 1] = t1[li - 1][ri - 1] + 1;
    }

    let mut t2 = Vec::with_capacity(n);
    for _ in 0..n {
        t2.push(vec![0 as i32; n]);
    }
    for i in 0..n {
        for j in 0..n {
            t2[i][j] = match (i > 0, j > 0) {
                (false, false) => t1[i][j],
                (false, true) => t1[i][j] + t2[i][j - 1],
                (true, false) => t1[i][j] + t2[i - 1][j],
                (true, true) => t1[i][j] + t2[i - 1][j] + t2[i][j - 1] - t2[i - 1][j - 1],
            };
        }
    }

    // for l in &t1 {
    //     println!("{:?}", l);
    // }
    // println!("----------");
    // for l in &t2 {
    //     println!("{:?}", l);
    // }

    for i in 0..q {
        let pi: usize = sc.read().unwrap();
        let pi = pi - 1;
        let qi: usize = sc.read().unwrap();
        let qi = qi - 1;

        if pi == 0 {
            println!("{}", t2[qi][qi]);
        } else {
            println!(
                "{}",
                t2[qi][qi] - t2[qi][pi - 1] - t2[pi - 1][qi] + t2[pi - 1][pi - 1]
            );
        }
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
