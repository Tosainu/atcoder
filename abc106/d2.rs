use std::cmp::{self, Ordering};
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let q: usize = sc.read().unwrap();

    let mut t = Vec::with_capacity(n);
    for _ in 0..n {
        t.push(vec![0; n]);
    }

    for _ in 0..m {
        let li: usize = sc.read().unwrap();
        let ri: usize = sc.read().unwrap();
        t[li - 1][ri - 1] = t[li - 1][ri - 1] + 1;
    }

    let t = t;
    // println!("{:?}", t);

    for _ in 0..q {
        let pi: usize = sc.read().unwrap();
        let pi = pi - 1;
        let qi: usize = sc.read().unwrap();
        let qi = qi - 1;

        let mut ans: usize = 0;
        for p in pi..qi + 1 {
            for q in p..qi + 1 {
                ans = ans + t[p][q];
            }
        }
        println!("{}", ans);
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
