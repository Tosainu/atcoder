use std::cmp::{self, Ordering};
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let q: usize = sc.read().unwrap();

    let mut lr = Vec::with_capacity(m);
    for _ in 0..m {
        let li: i32 = sc.read().unwrap();
        let ri: i32 = sc.read().unwrap();
        lr.push((li, ri));
    }
    lr.sort();
    let lr = lr;

    // println!("{:?}", lr);

    for _ in 0..q {
        let pi: i32 = sc.read().unwrap();
        let qi: i32 = sc.read().unwrap();

        let r = lr.binary_search_by_key(&(pi - 1), |&(l, r)| l);
        let r = match r {
            Ok(i) | Err(i) => i,
        };

        let mut ans = 0;
        for i in r..m {
            let v = lr[i];
            if v.0 > qi {
                break;
            }
            if v.0 >= pi && v.1 <= qi {
                ans = ans + 1;
            }
        }
        println!("{}", ans);
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
