#include <algorithm>
#include <deque>
#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

char board[1000][1000];

auto main() -> int {
  int h, w;
  std::cin >> h >> w >> std::ws;
  {
    std::vector<char> b{std::istreambuf_iterator<char>(std::cin), std::istreambuf_iterator<char>()};
    for (auto i = 0; i < h; ++i) {
      for (auto j = 0; j < w; ++j) {
        board[i][j] = b[i * (w + 1) + j];
      }
    }
  }

#ifdef DEBUG
  for (auto i = 0u; i < h; ++i) {
    std::cout << board[i] << std::endl;
  }
#endif

  using pii = std::pair<int, int>;
  using tiii = std::tuple<int, int, int>;

  auto q = std::deque<tiii>{};

  // start points
  for (auto i = 0; i < h; ++i) {
    for (auto j = 0; j < w; ++j) {
      if (board[i][j] == '#') {
        q.emplace_back(i, j, 0);
      }
    }
  }

  int r = 0;

  while (!q.empty()) {
    const auto p = q.front();
    q.pop_front();

    const auto p2 = std::get<2>(p);

    for (const pii& d : {pii{-1, 0}, pii{0, -1}, pii{0, 1}, pii{1, 0}}) {
      const auto i = std::get<0>(p) + d.first;
      const auto j = std::get<1>(p) + d.second;
      if (i < 0 || j < 0 || i >= h || j >= w || board[i][j] == '#') continue;

      board[i][j] = '#';
      q.emplace_back(i, j, p2 + 1);

      r = p2 + 1;

#ifdef DEBUG
      std::cout << "> " << r << " (" << i << ", " << j << ")" << std::endl;

      for (auto i = 0u; i < h; ++i) {
        std::cout << board[i] << std::endl;
      }
#endif
    }
  }

  std::cout << r << std::endl;
}
