#include <algorithm>
#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

template <class T>
T mins(T a, T b) {
  return std::min(a, b);
}

template <class T, class... Ts>
T mins(T a, T b, Ts... args) {
  return mins(std::min(a, b), args...);
}

class board {
  std::int64_t height_;
  std::int64_t width_;

  std::int64_t r_, c_;

public:
  board(std::int64_t height, std::int64_t width, std::int64_t r, std::int64_t c)
      : height_(height), width_(width), r_(r), c_(c) {}

  bool finished() const {
    return r_ < 1 || c_ < 1 || r_ > height_ || c_ > width_;
  }

  void print_pos() {
    std::cout << '(' << r_ << ", " << c_ << ')' << std::endl;
  }

  std::tuple<std::int64_t, std::int64_t> move(char sn) const {
    auto r = r_;
    auto c = c_;
    switch (sn) {
      case 'L':
        c--;
        break;
      case 'R':
        c++;
        break;
      case 'U':
        r--;
        break;
      case 'D':
        r++;
        break;
      default:
        std::cout << "invalid char (" << sn << ")" << std::endl;
        break;
    }
    return {r, c};
  }

  std::int64_t score(std::int64_t r, std::int64_t c) const {
    return mins(r - 1, c - 1, height_ - r, width_ - c);
  }

  void takahashi(char si) {
    auto ps = score(r_, c_);
    std::int64_t nr, nc;
    std::tie(nr, nc) = move(si);
    auto ns = score(nr, nc);
    if (ns <= ps) {
      r_ = nr;
      c_ = nc;

#ifdef DEBUG
      std::cout << "takahashi: move (" << ps << " -> " << ns << ")" << std::endl;
      print_pos();
    } else {
      std::cout << "takahashi: stay" << std::endl;
#endif
    }
  }

  void aoki(char si) {
    auto ps = score(r_, c_);
    std::int64_t nr, nc;
    std::tie(nr, nc) = move(si);
    auto ns = score(nr, nc);
    if (ns >= ps) {
      r_ = nr;
      c_ = nc;

#ifdef DEBUG
      std::cout << "aoki: move (" << ps << " -> " << ns << ")" << std::endl;
      print_pos();
    } else {
      std::cout << "aoki: stay" << std::endl;
#endif
    }
  }
};

auto main() -> int {
  std::int64_t h, w, n, sr, sc;
  std::cin >> h >> w >> n;
  std::cin >> sr >> sc >> std::ws;

  auto b = board{h, w, sr, sc};

  // auto s = std::vector<char>(n + 1);
  // auto t = std::vector<char>(n + 1);

  auto s = std::string{};
  auto t = std::string{};

  std::getline(std::cin, s);
  std::getline(std::cin, t);

  // std::cin.get();
  // std::copy_n(std::istreambuf_iterator<char>{std::cin}, n + 1, s.begin());
  // std::cin.get();
  // // std::cin.get();
  // std::copy_n(std::istreambuf_iterator<char>{std::cin}, n + 1, t.begin());
  // // std::cin.get();

#ifdef DEBUG
  for (auto&& c: s) {
    std::cout << c;
  }
  std::cout << std::endl;

  for (auto&& c: t) {
    std::cout << c;
  }
  std::cout << std::endl;

  b.print_pos();
#endif

  for (std::int64_t i = 0; !b.finished() && i < n * 2; ++i) {
    if (i % 2) {
      b.aoki(t[i / 2]);
    } else {
      b.takahashi(s[i / 2]);
    }
  }

  std::cout << (b.finished() ? "NO" : "YES") << std::endl;
}
