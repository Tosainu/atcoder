use std::cmp;
use std::collections::*;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let h: usize = sc.read().unwrap();
    let w: usize = sc.read().unwrap();
    let mut b = sc.read_board(h).unwrap();

    let mut q = VecDeque::new();

    for y in (0..h) {
        for x in (0..w) {
            if b[y][x] == '#' {
                q.push_back((x, y, 0));
            }
        }
    }

    let mut r = 0;
    while let Some((sx, sy, n)) = q.pop_front() {
        for &(dx, dy) in [(0, -1), (-1, 0), (1, 0), (0, 1)].iter() {
            let x = sx as i64 + dx;
            let y = sy as i64 + dy;

            if x < 0 || y < 0 || x >= w as _ || y >= h as _ {
                continue;
            }

            if b[y as usize][x as usize] == '#' {
                continue;
            }

            b[y as usize][x as usize] = '#';
            q.push_back((x as usize, y as usize, n + 1));

            r = n + 1;
        }
    }

    println!("{}", r);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
