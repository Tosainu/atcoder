use std::cmp::{self, Ordering};
use std::collections::*;
use std::io::{self, Read};

const MOD: usize = 1000000007;

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let a: Vec<usize> = sc.read_vec(n).unwrap();

    let mut t = vec![0 as usize; n];
    t[0] = a[0];
    for i in 1..n {
        t[i] = a[i] + t[i - 1];
    }

    let mut ans: usize = 0;
    for i in 0..n {
        for j in i..n {
            let r = if i == 0 { t[j] } else { t[j] - t[i - 1] };

            if r % m == 0 {
                ans = ans + 1;
            }
        }
        // println!("{}: {:?}", i, t);
    }
    println!("{}", ans);
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
