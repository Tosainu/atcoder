#![allow(unused_imports)]
use std::cmp::{self, Ord, Ordering};
use std::collections::{BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque};

fn main() {
    let stdin = std::io::stdin();
    let mut sc = nyan::Scanner::new(stdin.lock());

    let mut a: Vec<i32> = sc.read_vec(3);
    a.sort();

    println!("{}", (a[2] - a[1]).abs() + (a[0] - a[1]).abs());
}

#[allow(dead_code)]
mod nyan {
    use std::fmt::Debug;
    use std::io::Read;
    use std::iter::FromIterator;
    use std::str::FromStr;

    pub struct Scanner<R: Read> {
        reader: R,
    }

    impl<R: Read> Scanner<R> {
        pub fn new(reader: R) -> Scanner<R> {
            Scanner { reader: reader }
        }

        fn chars<'a>(&'a mut self) -> Box<Iterator<Item = char> + 'a> {
            Box::new(self.reader.by_ref().bytes().map(|b| b.unwrap() as char))
        }

        // since Rust 1.26
        // fn chars<'a>(&'a mut self) -> impl Iterator<Item = char> + 'a {
        //     self.reader.by_ref().bytes().map(|b| b.unwrap() as char)
        // }

        pub fn next<T: FromIterator<char>>(&mut self) -> T {
            self.chars()
                .skip_while(|c| c.is_whitespace())
                .take_while(|c| !c.is_whitespace())
                .collect()
        }

        pub fn next_line<T: FromIterator<char>>(&mut self) -> T {
            self.chars()
                .skip_while(|c| c.is_whitespace())
                .take_while(|c| !c.is_whitespace() || c == &' ')
                .collect()
        }

        pub fn read<T>(&mut self) -> T
        where
            T: FromStr,
            <T as FromStr>::Err: Debug,
        {
            self.next::<String>().parse().unwrap()
        }

        pub fn read_vec<T, U>(&mut self, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromStr,
            <U as FromStr>::Err: Debug,
        {
            (0..n).map(|_| self.read()).collect()
        }

        pub fn read_vec2d<T, U, V>(&mut self, m: usize, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromIterator<V>,
            V: FromStr,
            <V as FromStr>::Err: Debug,
        {
            (0..m).map(|_| self.read_vec(n)).collect()
        }

        pub fn read_line<T: FromIterator<char>>(&mut self) -> T {
            self.next_line()
        }

        pub fn read_lines<T, U>(&mut self, n: usize) -> T
        where
            T: FromIterator<U>,
            U: FromIterator<char>,
        {
            (0..n).map(|_| self.read_line()).collect()
        }
    }
}
