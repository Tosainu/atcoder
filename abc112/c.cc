#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::uint32_t N;
  std::cin >> N;

  std::uint32_t t = INF;

  std::vector<std::int32_t> xs(N);
  std::vector<std::int32_t> ys(N);
  std::vector<std::int64_t> hs(N);
  for (auto i = 0ul; i < N; ++i) {
    std::cin >> xs[i] >> ys[i] >> hs[i];
    if (t == INF && hs[i] >= 1) t = i;
  }

  for (auto cy = 0; cy <= 100; ++cy) {
    for (auto cx = 0; cx <= 100; ++cx) {
      // h_t = H − |x_t − c_x| − | y_t − c_y|
      // h   = h_t + |x_t − c_x| + | y_t − c_y|
      const auto h = hs[t] + std::abs(xs[t] - cx) + std::abs(ys[t] - cy);
      if (h < 1) continue;

      bool f = true;
      for (auto i = 0ul; i < N; ++i) {
        f = f && (std::max(h - std::abs(xs[i] - cx) - std::abs(ys[i] - cy), 0l) == hs[i]);
      }
      if (f) {
        std::cout << cx << ' ' << cy << ' ' << h << std::endl;
        return 0;
      }
    }
  }

  return -1;
}
