#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N, T;
  std::cin >> N >> T;

  int cost = INF;
  for (auto i = 0; i < N; ++i) {
    int ci, ti;
    std::cin >> ci >> ti;

    if (ti > T)
      continue;
    else
      cost = std::min(ci, cost);
  }

  if (cost == INF)
    std::cout << "TLE" << std::endl;
  else
    std::cout << cost << std::endl;
}
