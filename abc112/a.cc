#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N;
  std::cin >> N;

  if (N == 1) {
    std::cout << "Hello World" << std::endl;
  } else if (N == 2) {
    int A, B;
    std::cin >> A >> B;
    std::cout << (A + B) << std::endl;
  }
}
