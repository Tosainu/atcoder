#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  std::uint64_t N, Q;
  std::cin >> N >> Q;

  std::string S;
  std::cin >> S;

  // v[i]: S_i までの 'AC' の数
  std::vector<std::uint64_t> v(N);
  v[0] = 0;
  for (std::uint64_t i = 1u; i < N; ++i) {
    if (S[i - 1] == 'A' && S[i] == 'C') {
      v[i] = v[i - 1] + 1;
    } else {
      v[i] = v[i - 1];
    }
  }

  for (std::uint64_t i = 0u; i < Q; ++i) {
    std::uint64_t l, r;
    std::cin >> l >> r;
    std::cout << (v[r - 1] - v[l - 1]) << '\n';
  }
}
