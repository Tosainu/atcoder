#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>

bool atcg(char c) {
  return c == 'A' || c == 'T' || c == 'C' || c == 'G';
}

auto main() -> int {
  std::string S;
  std::cin >> S;

  const auto l = S.length();

  // DFS する
  std::deque<std::size_t> q{};

  // 初期の探索開始地点の登録
  for (auto i = 0u; i < l; ++i) {
    if (atcg(S[i])) q.emplace_back(i);
  }

  std::size_t f = 0;   // 探索済みインデックス
  std::size_t ans = 0; // 最長の部分文字列の長さ

  while (!q.empty()) {
    const auto s = q.front();
    q.pop_front();

    if (s < f) continue;

    // 何文字続くか
    std::size_t a = 1;
    auto i = s + 1;
    for (; atcg(S[i]) && i < l; ++i) {
      a++;
    }
    ans = std::max(ans, a);
    f = i;
  }

  std::cout << ans << std::endl;
}
