#include <algorithm>
#include <iostream>

auto main() -> int {
  char b;
  std::cin >> b;

  switch (b) {
    case 'A':
      std::cout << 'T';
      break;
    case 'T':
      std::cout << 'A';
      break;
    case 'C':
      std::cout << 'G';
      break;
    case 'G':
      std::cout << 'C';
      break;
  }
  std::cout << std::endl;
}
