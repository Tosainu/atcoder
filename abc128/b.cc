#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N;
  std::cin >> N;

  auto f = [](const auto& a, const auto& b) {
    auto& af = std::get<1>(a);
    auto& as = std::get<2>(a);
    auto& bf = std::get<1>(b);
    auto& bs = std::get<2>(b);
    return af == bf ? as < bs : af > bf;
  };
  using psi = std::tuple<int, std::string, int>; // index, name, point
  std::priority_queue<psi, std::vector<psi>, decltype(f)> q(f);

  for (auto i = 0; i < N; ++i) {
    std::string Si{};
    int Pi;
    std::cin >> Si >> Pi;
    q.emplace(i, std::move(Si), Pi);
  }

  while (!q.empty()) {
    const auto& v = q.top();
    std::cout << (std::get<0>(v) + 1) << std::endl;
    q.pop();
  }
}
