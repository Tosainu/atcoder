#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  int A, B, C;
  std::cin >> A >> B >> C;
  int ans = std::min(B / A, C);
  std::cout << ans << std::endl;
}
