#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  int A, B, K;
  std::cin >> A >> B >> K;

  int ans;
  int f = 0;

  int t = std::min(A, B);
  while (f != K) {
    if (A % t == 0 && B % t == 0) {
      f++;
      ans = t;
    }
    t--;
  }

  std::cout << ans << std::endl;
}
