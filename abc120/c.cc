#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  std::string S{};
  std::cin >> S;

  const auto L = S.length();
  const auto nb = static_cast<decltype(L)>(std::count(S.cbegin(), S.cend(), '1'));

  std::cout << (std::min(nb, L - nb) * 2) << std::endl;
}

// std::uint64_t ans = 0;

// bool modified;
// do {
//   modified = false;

//   for (auto s1 = S.begin(); s1 < S.end() - 1;) {
//     // 取り除かれたものはスキップ
//     if (*s1 == '-') {
//       ++s1;
//       continue;
//     }

//     auto s2 = s1 + 1;
//     for (; s2 < S.end(); ++s2) {
//       // 取り除かれたものはスキップ
//       if (*s2 == '-') {
//         continue;
//       }

//       if (*s1 != *s2) {
//         // 隣り合うキューブの色が違ったら取り除く
//         *s1 = '-';
//         *s2 = '-';
//         ans += 2;
//         modified = true;
//         s1 = s2 + 1;
//       } else {
//         // 前のキューブと同じ色だったらスキップ
//         s1 = s2;
//       }

//       // std::cout << S << std::endl;
//       break;
//     }
//   }
// } while (modified);

// std::cout << ans << std::endl;
