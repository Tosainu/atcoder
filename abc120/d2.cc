#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <iostream>
#include <memory>
#include <set>
#include <tuple>
#include <unordered_map>
#include <vector>

class union_find {
  const std::size_t n_;
  std::vector<std::size_t> root_;
  std::vector<std::size_t> rank_;
  std::vector<std::size_t> size_;

public:
  union_find(std::size_t n) : n_(n), root_(n), rank_(n_, 0u), size_(n_, 1u) {
    for (auto i = 0ul; i < n_; ++i) root_[i] = i;
  }

  std::size_t root(std::size_t v) {
    const auto r = root_[v];
    if (r == v) {
      return v;
    } else {
      return root_[v] = root(r);
    }
  }

  void merge(std::size_t x, std::size_t y) {
    const auto rx = root(x);
    const auto ry = root(y);
    if (rx == ry) return;

    if (rank_[rx] < rank_[ry]) {
      root_[rx] = ry;
      size_[ry] += size_[rx];
    } else {
      root_[ry] = rx;
      size_[rx] += size_[ry];
      if (rank_[rx] == rank_[ry]) rank_[rx]++;
    }
  }

  std::size_t size(std::size_t x) {
    return size_[root(x)];
  }

  bool is_same(std::size_t x, std::size_t y) {
    return root_[x] == root_[y];
  }
};

auto main() -> int {
  std::size_t N, M;
  std::cin >> N >> M;

  // M 本の橋
  std::vector<std::pair<std::size_t, std::size_t>> bridges(M);
  for (auto i = 0u; i < M; ++i) {
    auto& b = bridges[i];
    auto& Ai = b.first;
    auto& Bi = b.second;
    std::cin >> Ai >> Bi;
  }

  // Union-Find Tree
  union_find t{N + 1};
  {
    const auto& b = bridges.back();
    t.merge(b.first, b.second);
  }

  std::vector<std::size_t> ans(M);
  ans.at(0) = (N * (N - 1)) / 2;

  for (auto i = 1ul; i < M; ++i) {
    auto& b = bridges.at(M - i - 1);
    const auto Ai = b.first;
    const auto Bi = b.second;

    if (!t.is_same(Ai, Bi)) {
      const auto na = t.size(Ai);
      const auto nb = t.size(Bi);
      ans.at(i) = ans.at(i - 1) - na * nb;
      t.merge(Ai, Bi);
    } else {
      ans.at(i) = ans.at(i - 1);
    }
  }

  for (auto it = ans.crbegin(); it < ans.crend(); ++it) {
    std::cout << *it << '\n';
  }
}
