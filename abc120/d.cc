#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <iostream>
#include <memory>
#include <vector>

// 10^5 x 10^5 の隣接行列
// using mat_type = std::array<std::bitset<100000u>, 100000u>;
using mat_type = std::array<std::bitset<4u>, 4u>;

inline auto at(mat_type& m, std::size_t a, std::size_t b) ->
    typename mat_type::value_type::reference {
  return m[b - 1][a - 1];
}

inline void set(mat_type& m, std::size_t a, std::size_t b) {
  at(m, a, b) = true;
  at(m, b, a) = true;
}

inline void clr(mat_type& m, std::size_t a, std::size_t b) {
  at(m, a, b) = false;
  at(m, b, a) = false;
}

void print(const mat_type& m) {
  for (const auto& b : m) {
    std::cout << b << std::endl;
  }
}

auto main() -> int {
  auto pm = std::make_unique<mat_type>();
  auto& mat = *pm;

  std::size_t N, M;
  std::cin >> N >> M;

  // M 本の橋
  std::vector<std::pair<std::size_t, std::size_t>> bridges(M);

  for (auto i = 0u; i < M; ++i) {
    auto& b = bridges[i];
    auto& Ai = b.first;
    auto& Bi = b.second;
    std::cin >> Ai >> Bi;

    // 島 Ai と Bi はつながっている
    set(mat, Ai, Bi);
  }

  print(mat);

  for (const auto& b : bridges) {
    std::cout << "--------------------------------" << std::endl;
    clr(mat, b.first, b.second);

    print(mat);
  }

  // std::cout << ans << std::endl;
}
