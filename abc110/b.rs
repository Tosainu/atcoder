use std::cmp;
use std::io::{self, Read};

fn main() {
    let stdin = {
        let mut s = String::new();
        io::stdin().read_to_string(&mut s).unwrap();
        s
    };
    let mut sc = Scanner::new(&stdin);

    let n: usize = sc.read().unwrap();
    let m: usize = sc.read().unwrap();
    let x: i32 = sc.read().unwrap();
    let y: i32 = sc.read().unwrap();
    let xs: Vec<i32> = sc.read_vec(n).unwrap();
    let ys: Vec<i32> = sc.read_vec(m).unwrap();

    let xmax = xs.iter().max().unwrap();
    let ymin = ys.iter().min().unwrap();

    if ymin > xmax && ymin > &x && ymin <= &y {
        println!("No War");
    } else {
        println!("War");
    }
}

struct Scanner<'a> {
    it: std::str::SplitWhitespace<'a>,
}

impl<'a> Scanner<'a> {
    fn new(s: &'a String) -> Scanner<'a> {
        Scanner {
            it: s.split_whitespace(),
        }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.it.next().and_then(|s| s.parse().ok())
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }
}
