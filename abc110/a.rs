use std::cmp;
use std::io::{self, Read};

fn main() {
    let stdin = {
        let mut s = String::new();
        io::stdin().read_to_string(&mut s).unwrap();
        s
    };
    let mut sc = Scanner::new(&stdin);

    let mut abc: Vec<i32> = sc.read_vec(3).unwrap();
    abc.sort_by(|a, b| b.cmp(a));

    let ans = abc[0] * 10 + abc[1] + abc[2];

    println!("{}", ans);
}

struct Scanner<'a> {
    it: std::str::SplitWhitespace<'a>,
}

impl<'a> Scanner<'a> {
    fn new(s: &'a String) -> Scanner<'a> {
        Scanner {
            it: s.split_whitespace(),
        }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.it.next().and_then(|s| s.parse().ok())
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }
}
