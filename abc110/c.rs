use std::cmp;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let s: String = sc.read().unwrap();
    let t: String = sc.read().unwrap();

    let mut table1: [char; 0x7f] = ['\0'; 0x7f];
    let mut table2: [char; 0x7f] = ['\0'; 0x7f];
    for (cs, ct) in s.chars().zip(t.chars()) {
        let is = cs as usize;
        let it = ct as usize;

        if table1[is] != '\0' && table1[is] != ct {
            println!("No");
            return;
        }

        if table2[it] != '\0' && table2[it] != cs {
            println!("No");
            return;
        }

        if table1[is] == '\0' {
            table1[is] = ct;
        }

        if table2[it] == '\0' {
            table2[it] = cs;
        }
    }

    println!("Yes");
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }
}
