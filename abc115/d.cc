#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

constexpr std::uint64_t INF = 1ull << 50;

auto main() -> int {
  std::uint64_t N, K;
  std::cin >> N >> K;

  std::vector<std::uint64_t> hs(N);
  for (auto& hi : hs) {
    std::cin >> hi;
  }
  std::sort(hs.begin(), hs.end());

  std::uint64_t ans = INF;
  for (auto i = 0ul; i <= (N - K); ++i) {
    const auto hmin = hs.at(i);
    const auto hmax = hs.at(i + K - 1);
    ans = std::min(hmax - hmin, ans);
  }

  std::cout << ans << std::endl;
}
