#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int N;
  std::cin >> N;

  std::vector<int> ps(N);
  for (auto& pi : ps) {
    std::cin >> pi;
  }
  std::sort(ps.begin(), ps.end(), std::greater<int>{});

  int ans = std::accumulate(ps.cbegin() + 1, ps.cend(), ps.front() / 2);
  std::cout << ans << std::endl;
}
