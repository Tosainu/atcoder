#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  int D;
  std::cin >> D;
  if (D <= 25) std::cout << "Christmas";
  if (D <= 24) std::cout << " Eve";
  if (D <= 23) std::cout << " Eve";
  if (D <= 22) std::cout << " Eve";
  std::cout << std::endl;
}
