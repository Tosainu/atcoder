#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

bool shichi_go_san(std::uint64_t n) {
  bool shichi = false, go = false, san = false;
  for (auto i = n; i != 0; i /= 10) {
    switch (i % 10) {
      case 7:
        shichi = true;
        break;
      case 5:
        go = true;
        break;
      case 3:
        san = true;
        break;
      default:
        return false;
    }
  }
  return shichi && go && san;
}

//    1 ~  1000: 357 375 537 573 735 753
//      ~  4999: 3357 3375 3537 3573 3735 3753
//      ~  6999: 5357 5375 5537 5573 5735 5753
//      ~ 39999: 33357 33375 33537 33573 33735 33753

auto main() -> int {
  std::uint64_t N;
  std::cin >> N;

  std::uint64_t ans = 0;
  for (auto i = 357ull; i <= N; ++i) {
    if (shichi_go_san(i)) {
      // std::cout << i << std::endl;
      ans++;
    }
  }
  std::cout << ans << std::endl;
}
