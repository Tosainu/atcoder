#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <deque>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <unordered_set>
#include <unordered_map>
#include <vector>

// constexpr std::int64_t INF = 1ull << 50;
constexpr int INF = 1 << 20;

auto main() -> int {
  std::string S;
  std::cin >> S;

  const auto L = S.length();

  int ans = INF;
  for (auto i = 0u; i < (L - 2); ++i) {
    const auto X = std::stoi(S.substr(i, 3));
    ans = std::min(std::abs(X - 753), ans);
  }
  std::cout << ans << std::endl;
}
