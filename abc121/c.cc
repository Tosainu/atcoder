#include <algorithm>
#include <cstdint>
#include <iostream>
#include <queue>
#include <deque>
#include <vector>

auto main() -> int {
  std::uint64_t N, M;
  std::cin >> N >> M;

  using value_type = std::pair<std::uint64_t, std::uint64_t>;
  auto cmp = [](const auto& a, const auto& b) { return a.first > b.first; };
  std::priority_queue<value_type, std::vector<value_type>, decltype(cmp)> q(cmp);

  for (auto i = 0ul; i < N; ++i) {
    std::uint64_t Ai, Bi;
    std::cin >> Ai >> Bi;
    q.emplace(Ai, Bi);
  }

  std::uint64_t rem = M;
  std::uint64_t total = 0;
  while (rem != 0 || !q.empty()) {
    const auto v = q.top();
    q.pop();

    const auto n = std::min(v.second, rem);

    total += v.first * n;
    rem -= n;
  }

  std::cout << total << std::endl;
}
