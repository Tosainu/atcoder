#include <algorithm>
#include <cstdint>
#include <iostream>
#include <queue>
#include <deque>
#include <vector>

//  0 0000
//  1 0001
//  2 0010
//  3 0011
//  4 0100
//  5 0101
//  6 0110
//  7 0111
//  8 1000
//  9 1001
// 10 1010
// 11 1011
// 12 1100
// 13 1101
// 14 1110
// 15 1111

auto main() -> int {
  std::uint64_t A, B;
  std::cin >> A >> B;

  std::uint64_t ans = 0;
  for (auto i = 0u; i < 8u; ++i) {
  // for (auto i = 0u; i < 63u; ++i) {
    const auto a = std::uint64_t{1} << (i);
    const auto b = std::uint64_t{1} << (i + 1);
    const auto x = a * (A / b);
    const auto y = a * (B / b);

    std::cout << i << ' ' << x << ' ' << y << std::endl;

    // ans |= static_cast<std::uint64_t>((y - x) % 2 == 1) << i;
  }

  // std::cout << ans << std::endl;
}
