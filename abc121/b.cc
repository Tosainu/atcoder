#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

int As[20][20];
int Bs[20];

auto main() -> int {
  int N, M, C;
  std::cin >> N >> M >> C;

  for (auto i = 0; i < M; ++i) {
    std::cin >> Bs[i];
  }

  for (auto i = 0; i < N; ++i) {
    for (auto j = 0; j < M; ++j) {
      std::cin >> As[i][j];
    }
  }

  int ans = 0;
  for (auto i = 0; i < N; ++i) {
    int a = 0;
    for (auto j = 0; j < M; ++j) {
      a += As[i][j] * Bs[j];
    }

    if (a > (-C)) {
      ans++;
    }
  }

  std::cout << ans << std::endl;
}
