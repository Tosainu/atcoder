#include <algorithm>
#include <cstdint>
#include <iostream>
#include <deque>
#include <vector>

auto main() -> int {
  int H, W, h, w;
  std::cin >> H >> W >> h >> w;

  std::cout << ((H - h) * (W - w)) << std::endl;
}
