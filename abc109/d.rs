use std::cmp;
use std::collections::*;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let h: usize = sc.read().unwrap();
    let w: usize = sc.read().unwrap();

    let mut b = Vec::with_capacity(h);
    for _ in 0..h {
        let mut bb = Vec::with_capacity(w);
        for _ in 0..w {
            let ayx: u8 = sc.read().unwrap();
            bb.push(ayx);
        }
        b.push(bb);
    }

    let mut m: Option<(usize, usize)> = None;
    let mut r = Vec::new();
    for i in 0..h {
        for jj in 0..w {
            let j = if i % 2 == 0 { jj } else { w - jj - 1 };
            // println!("{:?}", (j, i));

            if let &Some((pi, pj)) = &m {
                // 移動中
                r.push((pi, pj, i, j));
                if b[i][j] % 2 == 0 {
                    m = Some((i, j));
                } else {
                    m = None;
                }
            } else if b[i][j] % 2 == 1 {
                // 移動開始
                m = Some((i, j));
            }
        }
    }

    println!("{}", r.len());
    for (pi, pj, i, j) in r {
        println!("{} {} {} {}", pi + 1, pj + 1, i + 1, j + 1);
    }
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
