use std::cmp;
use std::collections::*;
use std::io::{self, Read};

fn main() {
    let stdin = io::stdin();
    let mut sc = Scanner::new(stdin.lock());

    let n: usize = sc.read().unwrap();

    let mut s = HashSet::new();
    let w: String = sc.read().unwrap();
    let w: Vec<char> = w.chars().collect();
    let mut wp = *w.last().unwrap();
    s.insert(w);

    for _ in 0..n - 1 {
        let wi: String = sc.read().unwrap();
        let wi: Vec<char> = wi.chars().collect();
        let ws = *wi.first().unwrap();

        if wp == ws && !s.contains(&wi) {
            wp = *wi.last().unwrap();
            s.insert(wi);
        } else {
            println!("No");
            return;
        }
    }

    println!("Yes");
}

struct Scanner<R: Read> {
    reader: R,
}

impl<R: Read> Scanner<R> {
    fn new(reader: R) -> Scanner<R> {
        Scanner { reader: reader }
    }

    fn read<T: std::str::FromStr>(&mut self) -> Option<T> {
        self.reader
            .by_ref()
            .bytes()
            .map(|b| b.unwrap() as char)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
            .parse()
            .ok()
    }

    fn read_vec<T: std::str::FromStr>(&mut self, n: usize) -> Option<Vec<T>> {
        (0..n).map(|_| self.read()).collect()
    }

    fn read_vec2d<T: std::str::FromStr>(&mut self, m: usize, n: usize) -> Option<Vec<Vec<T>>> {
        (0..m).map(|_| self.read_vec(n)).collect()
    }

    fn read_board(&mut self, n: usize) -> Option<Vec<Vec<char>>> {
        (0..n)
            .map(|_| {
                self.read::<String>()
                    .map(|s| s.chars().collect::<Vec<char>>())
            })
            .collect()
    }
}
